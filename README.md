# Tools
* [SimulIDE - Real Time Electronic Circuit Simulator. With PIC, AVR and Arduino simulation](http://simulide.blogspot.com/)
* [Tinkercad](https://www.tinkercad.com/)
* [CIRCUITO.IO](https://www.circuito.io/)
* [LCD Smartie](http://lcdsmartie.sourceforge.net/)

# Hardware
* [Project Ubertooth](http://ubertooth.sourceforge.net/) / [Android Ubertooth software](https://github.com/mobilesec/android-ubertooth-btle-sniffing)

# Projects
* [AVR PAL generation ](http://trznadel.info/kuba/avr/index2.php)
 
# Portals/WebSites
* [PL / FORBOT](https://forbot.pl/blog/)
* [Radio, Electronics and Computing Projects by Hans Summers](http://www.hanssummers.com/)

# Arduino
* [Arduino Programming Cheat Sheet](https://i.redd.it/8m27xkluynd31.png)
 
# Varia
* [Collapse OS](https://collapseos.org/)